FROM openjdk:22-jdk
LABEL authors="ahmadroni"
MAINTAINER pkp.com
COPY target/order-service-0.0.1-SNAPSHOT.jar order-service-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/order-service-0.0.1-SNAPSHOT.jar"]