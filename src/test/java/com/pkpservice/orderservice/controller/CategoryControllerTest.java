package com.pkpservice.orderservice.controller;

import com.pkpservice.orderservice.model.response.CategoryResponse;
import com.pkpservice.orderservice.service.CategoryService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
class CategoryControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CategoryService service;

    @BeforeEach
    void setUp() {
    }

    @Test
    void getAll() throws Exception {
        Mockito.lenient().when(service.getAll()).thenReturn(Collections.emptyList());
        mockMvc.perform(get("/api/v1/category"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.statusCode").exists())
                .andExpect(jsonPath("$.message").exists())
                .andExpect(jsonPath("$.data").exists())
                .andReturn();
    }

    @Test
    void getPage_WhenGivenEmptyArray() throws Exception {
        Mockito.lenient().when(service.getAll()).thenReturn(Collections.emptyList());
        mockMvc.perform(get("/api/v1/category/page/0"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.statusCode").exists())
                .andExpect(jsonPath("$.message").exists())
                .andExpect(jsonPath("$.data").isEmpty())
                .andReturn();
    }

    @Test
    void getPage_WhenGivenNotEmptyArray() throws Exception {
        List<CategoryResponse> result = new ArrayList<>();
        result.add(new CategoryResponse(1L,"C001","Makanan", null));
        result.add(new CategoryResponse(2L,"C002","Minuman", null));
        result.add(new CategoryResponse(3L,"C003","Fashion", null));
        Mockito.lenient().when(service.getAll()).thenReturn(result);

        mockMvc.perform(get("/api/v1/category"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.statusCode").exists())
                .andExpect(jsonPath("$.message").exists())
                .andExpect(jsonPath("$.data").exists())
                .andExpect(jsonPath("$.data[0].id").exists())
                .andExpect(jsonPath("$.data[0].name").exists())
                .andExpect(jsonPath("$.data[0].code").exists())
                .andReturn();
    }

    @Test
    void testGetPage() {
    }

    @Test
    void getById() {
    }

    @Test
    void save() {
    }

    @Test
    void update() {
    }

    @Test
    void delete() {
    }
}