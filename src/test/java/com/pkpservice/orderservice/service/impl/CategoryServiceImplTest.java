package com.pkpservice.orderservice.service.impl;

import com.pkpservice.orderservice.model.entity.CategoryEntity;
import com.pkpservice.orderservice.model.response.CategoryResponse;
import com.pkpservice.orderservice.repository.CategoryRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
class CategoryServiceImplTest {
    @Autowired
    @InjectMocks
    private CategoryServiceImpl service;

    @Mock
    private CategoryRepository repository;

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void calculatorTest() {
        int number = 10;
        Assertions.assertNotNull(number);
        Assertions.assertEquals(10, number);
    }

    @Test
    void getPage() {
    }

    @Test
    void testGetPage() {
    }

    @Test
    void getAll_WhenGivenEmptyArray() {
        List<CategoryEntity> entities = new ArrayList<>();
        Mockito.when(repository.findAll()).thenReturn(entities);

        List<CategoryResponse> result = service.getAll();
        assertNotNull(result);
        assertTrue(result.isEmpty());
        assertEquals(0, result.size());
    }

    @Test
    void getAll_WhenGivenNotEmptyArray() {
        List<CategoryEntity> entities = new ArrayList<>();
        entities.add(new CategoryEntity("C001","Makanan"));
        entities.add(new CategoryEntity("C002","Minuman"));
        entities.add(new CategoryEntity("C003","Fashion"));
        Mockito.when(repository.findAll()).thenReturn(entities);

        List<CategoryResponse> result = service.getAll();
        assertNotNull(result);
        assertFalse(result.isEmpty());
        assertEquals(3, result.size());
    }

    @Test
    void getById() {
    }

    @Test
    void save() {
    }

    @Test
    void update() {
    }

    @Test
    void delete() {
    }
}