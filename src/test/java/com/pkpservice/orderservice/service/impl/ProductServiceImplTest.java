package com.pkpservice.orderservice.service.impl;

import com.pkpservice.orderservice.model.entity.ProductEntity;
import com.pkpservice.orderservice.model.request.ProductRequest;
import com.pkpservice.orderservice.repository.CategoryRepository;
import com.pkpservice.orderservice.repository.ProductRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
class ProductServiceImplTest {
    @Autowired
    @InjectMocks
    private ProductServiceImpl service;

    @Mock
    private CategoryRepository categoryRepo;
    @Mock
    private ProductRepository productRepo;

    @BeforeEach
    void setUp() {
    }

    @Test
    void getPages() {
    }

    @Test
    void getAll() {
    }

    @Test
    void getById() {
    }

    @Test
    void save() {
        ProductRequest request = new ProductRequest(1L,"Product Test","Description", 1_000L,1L,100L);
        Mockito.lenient().when(categoryRepo.findById(request.getCategoryId())).thenReturn(Optional.empty());

        ProductEntity entity = new ProductEntity();
        BeanUtils.copyProperties(request, entity);
        Mockito.lenient().when(productRepo.save(entity)).thenReturn(entity);

        var result = service.save(request);
        assertNotNull(result);
    }

    @Test
    void update() {
    }

    @Test
    void delete() {
    }
}