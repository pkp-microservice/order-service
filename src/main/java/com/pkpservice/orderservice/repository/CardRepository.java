package com.pkpservice.orderservice.repository;

import com.pkpservice.orderservice.model.entity.CartEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CardRepository extends JpaRepository<CartEntity, Long> {
    List<CartEntity> findByCustomerId(String customerId);
    Optional<CartEntity> findByInvoiceNo(String invoiceNo);

    boolean existsByInvoiceNo(String invoiceNo);
}
