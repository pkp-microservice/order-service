package com.pkpservice.orderservice.controller;

import com.pkpservice.orderservice.model.entity.CartEntity;
import com.pkpservice.orderservice.model.entity.ProductEntity;
import com.pkpservice.orderservice.model.request.CartRequest;
import com.pkpservice.orderservice.model.response.Response;
import com.pkpservice.orderservice.repository.ProductRepository;
import com.pkpservice.orderservice.service.CartService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/cart")
@RequiredArgsConstructor
public class CartController {
    private final CartService cartService;
    private final ProductRepository productRepo;

    @GetMapping("")
    public ResponseEntity<Response> getAll(){
        var result = this.cartService.getAll();
        return ResponseEntity.ok().body(
                new Response(200,"SUCCESS", result)
        );
    }

    @GetMapping(value = "/product", produces = {"application/json"})
    public ResponseEntity<Response> getProduct(){
        List<ProductEntity> result = this.productRepo.findAll();
        return ResponseEntity.ok().body(
                new Response(200,"SUCCESS", result)
        );
    }



    @GetMapping("/{id}")
    public ResponseEntity<Response> getById(@PathVariable("id") Long id){
        return ResponseEntity
                .ok()
                .body(
                        new Response(200,"SUCCESS", this.cartService.getById(id))
                );
    }

    @PostMapping("")
    public ResponseEntity<Response> save(@RequestBody CartRequest request){
        var result = this.cartService.save(request).orElse(null);
        return ResponseEntity
                .ok()
                .body(
                        new Response(200,"SUCCESS", result)
                );

    }

    @PostMapping("/simple")
    public ResponseEntity<Response> simpleSave(@RequestBody CartRequest request){
        var result = this.cartService.simpleSave(request).orElse(null);
        return ResponseEntity
                .ok()
                .body(
                        new Response(200,"SUCCESS", result)
                );

    }

    @GetMapping("/customer/{id}")
    public ResponseEntity<Response> getByCustomerId(@PathVariable("id") String id){
        var result = this.cartService.getByCustomerId(id);
        return ResponseEntity
                .ok()
                .body(
                        new Response(200,"SUCCESS", result)
                );

    }

    @GetMapping("/invoice/{invoiceNo}")
    public ResponseEntity<Response> getByInvoiceNo(@PathVariable("invoiceNo") String invoiceNo){
        var result = this.cartService.getByInvoiceNo(invoiceNo);
        return ResponseEntity
                .ok()
                .body(
                        new Response(200,"SUCCESS", result)
                );

    }
}
