package com.pkpservice.orderservice.controller;

import com.pkpservice.orderservice.model.request.ProductRequest;
import com.pkpservice.orderservice.model.response.ProductResponse;
import com.pkpservice.orderservice.model.response.Response;
import com.pkpservice.orderservice.service.ProductService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/product")
public class ProductController {
    private ProductService service;

    @Autowired
    public ProductController(ProductService service) {
        this.service = service;
    }

    @GetMapping
    public ResponseEntity<Response> getAll(){
        var result = this.service.getAll();
        return ResponseEntity.ok()
                .body(
                        new Response(200,"Success", result)
                );
    }

    @GetMapping("/page/{pageNo}")
    public ResponseEntity<Response> getPage(@PathVariable("pageNo") int page){
        var result = this.service.getPages(page);
        return ResponseEntity.ok()
                .body(
                        new Response(200,"Success", result)
                );
    }

    @GetMapping("/{id}")
    public ResponseEntity<Response> getById(@PathVariable("id") Long id){
        var result = this.service.getById(id).orElse(null);
        return ResponseEntity.ok()
                .body(
                        new Response(200,"Success", result)
                );
    }

    @PostMapping
    public ResponseEntity<Response> save(@RequestBody @Valid ProductRequest request){
        var result = this.service.save(request).orElse(null);
        return ResponseEntity.ok()
                .body(
                        new Response(200,"Success", result)
                );
    }

    @PatchMapping("/{id}")
    public ResponseEntity<Response> update(@RequestBody @Valid ProductRequest request,
                                           @PathVariable("id") Long id){
        var result = this.service.update(request, id).orElse(null);
        return ResponseEntity.ok()
                .body(
                        new Response(200,"Success", result)
                );
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Response> delete(@PathVariable("id") Long id){
        var result = this.service.delete(id).orElse(null);
        return ResponseEntity.ok()
                .body(
                        new Response(200,"Success", result)
                );
    }
}
