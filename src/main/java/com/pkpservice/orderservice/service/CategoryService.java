package com.pkpservice.orderservice.service;


import com.pkpservice.orderservice.model.request.CategoryRequest;
import com.pkpservice.orderservice.model.request.PageFilterRequest;
import com.pkpservice.orderservice.model.response.CategoryResponse;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Optional;

public interface CategoryService {
    Page<CategoryResponse> getPage(int page);
    Page<CategoryResponse> getPage(PageFilterRequest filter);
    List<CategoryResponse> getAll();
    Optional<CategoryResponse> getById(Long id);
    Optional<CategoryResponse> save(CategoryRequest request);
    Optional<CategoryResponse> update(CategoryRequest request, Long id);
    Optional<CategoryResponse> delete(Long id);
}
