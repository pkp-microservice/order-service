package com.pkpservice.orderservice.service;

import com.pkpservice.orderservice.model.request.CartRequest;
import com.pkpservice.orderservice.model.response.CartResponse;

import java.util.List;
import java.util.Optional;

public interface CartService {
    List<CartResponse> getAll();
    Optional<CartResponse> getById(Long id);
    List<CartResponse> getByCustomerId(String customerId);
    Optional<CartResponse> getByInvoiceNo(String invoiceNo);
    Optional<CartResponse> save(CartRequest request);
    Optional<CartResponse> simpleSave(CartRequest request);
}
