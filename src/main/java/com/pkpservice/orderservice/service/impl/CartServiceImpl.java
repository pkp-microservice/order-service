package com.pkpservice.orderservice.service.impl;

import com.pkpservice.orderservice.exception.CommonApiException;
import com.pkpservice.orderservice.model.entity.CartEntity;
import com.pkpservice.orderservice.model.entity.CartItemEntity;
import com.pkpservice.orderservice.model.request.CartItemRequest;
import com.pkpservice.orderservice.model.request.CartRequest;
import com.pkpservice.orderservice.model.response.CartResponse;
import com.pkpservice.orderservice.repository.CardRepository;
import com.pkpservice.orderservice.service.CartService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class CartServiceImpl implements CartService {
    private final CardRepository cardRepo;
    @Override
    public List<CartResponse> getAll() {
        var result = this.cardRepo.findAll();
        if(result.isEmpty()){
            return Collections.emptyList();
        }

        return result.stream()
                .map(CartResponse::new)
                .collect(Collectors.toList());
    }

    @Override
    public Optional<CartResponse> getById(Long id) {
        var result = this.cardRepo.findById(id).orElse(null);
        if(result == null){
            return Optional.empty();
        }

        return Optional.of(new CartResponse(result));
    }

    @Override
    public List<CartResponse> getByCustomerId(String customerId) {
        var result = this.cardRepo.findByCustomerId(customerId);
        if(result.isEmpty()){
            return Collections.emptyList();
        }
        return result.stream().map(CartResponse::new)
                .collect(Collectors.toList());
    }

    @Override
    public Optional<CartResponse> getByInvoiceNo(String invoiceNo) {
        var result = this.cardRepo.findByInvoiceNo(invoiceNo).orElse(null);
        if(result == null) {
            return Optional.empty();
        }
        return Optional.of(new CartResponse(result));
    }

    @Override
    public Optional<CartResponse> save(CartRequest request) {
        if(cardRepo.existsByInvoiceNo(request.getInvoiceNo())){
            throw new CommonApiException("Invoice no "+request.getInvoiceNo()+" is exist", HttpStatus.BAD_REQUEST);
        }

        CartEntity cart = new CartEntity();
        cart.setCustomerName(request.getCustomerName());
        cart.setInvoiceNo(request.getInvoiceNo());

        // total
        double total = 0.0;
        for(CartItemRequest itemDto: request.getCartItems()){
            CartItemEntity itemEntity = new CartItemEntity();
            itemEntity.setCartId(cart.getId());
            itemEntity.setProductId(itemDto.getProductId());
            itemEntity.setQty(itemDto.getQty());
            itemEntity.setPrice(itemDto.getPrice());
            itemEntity.setSubTotal(itemDto.getQty() * itemDto.getPrice());

            // add to list
            cart.addCartItem(itemEntity);

            total = total + itemEntity.getSubTotal();
        }
        // set total
        cart.setTotal(total);
        // set created at
        cart.setCreatedAt(LocalDateTime.now());

        try{
            this.cardRepo.save(cart);
            log.info("Save cart success");
            return Optional.of(new CartResponse(cart));
        }catch (Exception e){
            log.error("Save cart failed, Error: {}", e.getMessage());
            throw new CommonApiException("Save cart failed", HttpStatus.INTERNAL_SERVER_ERROR);
            //return Optional.empty();
        }
    }

    @Override
    public Optional<CartResponse> simpleSave(CartRequest request) {
        if(request == null) {
            log.info("Request is null");
            throw new CommonApiException("Request is empty", HttpStatus.BAD_REQUEST);
        }

        if(cardRepo.existsByInvoiceNo(request.getInvoiceNo())){
            log.info("Invoice no {} is exist", request.getInvoiceNo());
            throw new CommonApiException("Invoice no "+request.getInvoiceNo()+" is exist", HttpStatus.BAD_REQUEST);
        }

        CartEntity cart = new CartEntity(request);
        try{
            this.cardRepo.save(cart);
            log.info("Save cart success");
            return Optional.of(new CartResponse(cart));
        }catch (Exception e){
            log.error("Save cart failed, Error: {}", e.getMessage());
            throw new CommonApiException("Save cart failed", HttpStatus.INTERNAL_SERVER_ERROR);
            //return Optional.empty();
        }
    }
}
