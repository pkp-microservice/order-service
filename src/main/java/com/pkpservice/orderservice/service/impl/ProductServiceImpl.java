package com.pkpservice.orderservice.service.impl;

import com.pkpservice.orderservice.exception.CommonApiException;
import com.pkpservice.orderservice.model.entity.CategoryEntity;
import com.pkpservice.orderservice.model.entity.ProductEntity;
import com.pkpservice.orderservice.model.request.ProductRequest;
import com.pkpservice.orderservice.model.response.ProductResponse;
import com.pkpservice.orderservice.repository.CategoryRepository;
import com.pkpservice.orderservice.repository.ProductRepository;
import com.pkpservice.orderservice.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.*;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {
    private final ProductRepository repository;
    private final CategoryRepository categoryRepo;

    @Value("${app.page-size.product}")
    private int pageSize;

    @Override
    public Page<ProductResponse> getPages(int page) {
        Sort sort = Sort.by(Sort.Direction.ASC, "name");
        Pageable pageable = PageRequest.of(page,pageSize, sort);
        Page<ProductEntity> result = this.repository.findAll(pageable);

        if(result.getContent().isEmpty()){
            return new PageImpl<>(Collections.emptyList(), pageable, 0);
        }
        List<ProductResponse> responses = result.getContent().stream()
                .map(ProductResponse::new)
                .collect(Collectors.toList());

        return new PageImpl<>(responses, pageable, responses.size());
    }

    @Override
    public List<ProductResponse> getAll() {
        var result = this.repository.findAll();
        if(result.isEmpty()){
            return Collections.emptyList();
        }

        return result.stream().map(ProductResponse::new)
                .collect(Collectors.toList());
    }

    @Override
    public Optional<ProductResponse> getById(Long id) {
        ProductEntity entity = this.repository.findById(id).orElse(null);
        if(entity == null){
            return Optional.empty();
        }
        return Optional.of(new ProductResponse(entity));
    }

    @Override
    public Optional<ProductResponse> save(ProductRequest request) {
        CategoryEntity category = this.categoryRepo.findById(request.getCategoryId()).orElse(null);
        if(category == null){
            //return Optional.empty();
            Map<String, String> errors = Map.of("categoryId", request.getCategoryId() +"  not found");
            throw new CommonApiException(null, HttpStatus.BAD_REQUEST, errors);
        }

        ProductEntity entity = new ProductEntity(request);
        try {
            this.repository.saveAndFlush(entity);
            return Optional.of(new ProductResponse(entity));
        }catch (Exception e) {
            throw new CommonApiException("Failed save save to database", HttpStatus.INTERNAL_SERVER_ERROR);
            //return Optional.empty();
        }
    }

    @Override
    public Optional<ProductResponse> update(ProductRequest request, Long id) {
        CategoryEntity category = this.categoryRepo.findById(request.getCategoryId()).orElse(null);
        if(category == null){
            //return Optional.empty();

            Map<String, String> errors = Map.of("categoryId", request.getCategoryId() +"  not found");
            throw new CommonApiException(null, HttpStatus.BAD_REQUEST, errors);
        }

        // find data
        ProductEntity entity = this.repository.findById(id).orElse(null);
        if(entity == null) {
            throw new CommonApiException("product id "+ request.getId() +"  not found", HttpStatus.BAD_REQUEST);
            //return Optional.empty();
        }
        // update data
        BeanUtils.copyProperties(request, entity);
        // save ke database
        try {
            this.repository.saveAndFlush(entity);
            return Optional.of(new ProductResponse(entity));
        }catch (Exception e) {
            throw new CommonApiException("Failed update data to database", HttpStatus.INTERNAL_SERVER_ERROR);
            //return Optional.empty();
        }
    }

    @Override
    public Optional<ProductResponse> delete(Long id) {
        // find data
        ProductEntity entity = this.repository.findById(id).orElse(null);
        if(entity == null) {
            Map<String,String> error = Map.of("id", id +"  not found");
            throw new CommonApiException(null, HttpStatus.BAD_REQUEST, error);
            //return Optional.empty();
        }

        // save ke database
        try {
            this.repository.delete(entity);
            return Optional.of(new ProductResponse(entity));
        }catch (Exception e) {
            throw new CommonApiException("Failed delete data from database", HttpStatus.INTERNAL_SERVER_ERROR);
            //return Optional.empty();
        }
    }
}
