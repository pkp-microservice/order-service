package com.pkpservice.orderservice.service.impl;

import com.pkpservice.orderservice.exception.CommonApiException;
import com.pkpservice.orderservice.model.entity.CategoryEntity;
import com.pkpservice.orderservice.model.request.CategoryRequest;
import com.pkpservice.orderservice.model.request.PageFilterRequest;
import com.pkpservice.orderservice.model.response.CategoryResponse;
import com.pkpservice.orderservice.repository.CategoryRepository;
import com.pkpservice.orderservice.service.CategoryService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.*;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CategoryServiceImpl implements CategoryService {
    private CategoryRepository repository;
    @Value("${app.page-size.category}")
    private int pageSize;

    // constructor
    @Autowired
    public CategoryServiceImpl(CategoryRepository repository) {
        this.repository = repository;
    }

    @Override
    public Page<CategoryResponse> getPage(int page) {
        //Sort sort = Sort.by(Sort.Direction.ASC, "name");
        Pageable pageable = PageRequest.of(page,pageSize);
        return getCategoryResponses(pageable);
    }

    @Override
    public Page<CategoryResponse> getPage(PageFilterRequest filter) {
        Pageable pageable = PageRequest.of(filter.getPage(),filter.getPageSize());
        return getCategoryResponses(pageable);
    }

    private Page<CategoryResponse> getCategoryResponses(Pageable pageable) {
        Page<CategoryEntity> result = this.repository.findAll(pageable);

        if(result.getContent().isEmpty()){
            return new PageImpl<>(Collections.emptyList(), pageable, 0);
        }
        List<CategoryResponse> responses = result.getContent().stream()
                .map(CategoryResponse::new)
                .collect(Collectors.toList());

        return new PageImpl<>(responses, pageable, responses.size());
    }

    @Override
    public List<CategoryResponse> getAll() {
        List<CategoryEntity> result = this.repository.findAll();
        if(result.isEmpty()){
            return Collections.emptyList();
        }

        return result.stream().map(CategoryResponse::new)
                .collect(Collectors.toList());
    }

    @Override
    public Optional<CategoryResponse> getById(Long id) {
        CategoryEntity entity = this.repository.findById(id).orElse(null);
        if(entity == null){
            return Optional.empty();
        }
        return Optional.of(new CategoryResponse(entity));
    }

    @Override
    public Optional<CategoryResponse> save(CategoryRequest request) {
        CategoryEntity entity = new CategoryEntity(request);
        try {
            this.repository.saveAndFlush(entity);
            return Optional.of(new CategoryResponse(entity));
        }catch (Exception e) {
            throw new CommonApiException("Failed save to database", HttpStatus.INTERNAL_SERVER_ERROR);
            //return Optional.empty();
        }
    }

    @Override
    public Optional<CategoryResponse> update(CategoryRequest request, Long id) {
        // find data
        CategoryEntity entity = this.repository.findById(id).orElse(null);
        if(entity == null) {
            throw new CommonApiException("Product with id"+ id +" not found", HttpStatus.BAD_REQUEST);
            //return Optional.empty();
        }
        // update data
        BeanUtils.copyProperties(request, entity);
        // save ke database
        try {
            this.repository.saveAndFlush(entity);
            return Optional.of(new CategoryResponse(entity));
        }catch (Exception e) {
            throw new CommonApiException("Failed update to database", HttpStatus.INTERNAL_SERVER_ERROR);
            //return Optional.empty();
        }
    }

    @Override
    public Optional<CategoryResponse> delete(Long id) {
        // find data
        CategoryEntity entity = this.repository.findById(id).orElse(null);
        if(entity == null) {
            throw new CommonApiException("Product with id"+ id +" not found", HttpStatus.BAD_REQUEST);
            //return Optional.empty();
        }

        // save ke database
        try {
            this.repository.delete(entity);
            return Optional.of(new CategoryResponse(entity));
        }catch (Exception e) {
            throw new CommonApiException("Failed save to database", HttpStatus.INTERNAL_SERVER_ERROR);
            //return Optional.empty();
        }
    }
}
