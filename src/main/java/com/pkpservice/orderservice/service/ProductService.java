package com.pkpservice.orderservice.service;


import com.pkpservice.orderservice.model.request.ProductRequest;
import com.pkpservice.orderservice.model.response.ProductResponse;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Optional;

public interface ProductService {
    Page<ProductResponse> getPages(int page);
    List<ProductResponse> getAll();
    Optional<ProductResponse> getById(Long id);
    Optional<ProductResponse> save(ProductRequest request);
    Optional<ProductResponse> update(ProductRequest request, Long id);
    Optional<ProductResponse> delete(Long id);
}
