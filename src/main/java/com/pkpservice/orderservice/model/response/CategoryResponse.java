package com.pkpservice.orderservice.model.response;

import com.pkpservice.orderservice.model.entity.CategoryEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CategoryResponse {
    private Long id;
    private String code;
    private String name;
    private List<ProductResponse> products;

    public CategoryResponse(CategoryEntity entity) {
        BeanUtils.copyProperties(entity, this);
        if(!entity.getProducts().isEmpty()){
            this.products = entity.getProducts().stream()
                    .map(ProductResponse::new)
                    .collect(Collectors.toList());
        }
    }
}
