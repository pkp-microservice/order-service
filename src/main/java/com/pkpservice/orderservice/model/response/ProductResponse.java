package com.pkpservice.orderservice.model.response;

import com.pkpservice.orderservice.model.entity.ProductEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductResponse {
    private Long id;
    private String name;
    private String description;
    private Long price;
    private Long categoryId;
    private String categoryName;
    private Long stock;

    public ProductResponse(ProductEntity entity) {
        BeanUtils.copyProperties(entity, this);
        if(entity.getCategory() != null){
            this.categoryName = entity.getCategory().getName();
        }
    }
}
