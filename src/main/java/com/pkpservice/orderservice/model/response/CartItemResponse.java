package com.pkpservice.orderservice.model.response;

import com.pkpservice.orderservice.model.entity.CartItemEntity;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CartItemResponse {
    @NotNull
    private Long productId;
    private String productName;
    @NotNull
    private Long price;
    @NotNull
    private Long qty;
    @NotNull
    private String description;

    public CartItemResponse(CartItemEntity entity) {
        BeanUtils.copyProperties(entity, this);
        if(entity.getProduct() != null){
            this.productName = entity.getProduct().getName();
        }
    }
}
