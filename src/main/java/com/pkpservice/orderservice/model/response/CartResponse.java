package com.pkpservice.orderservice.model.response;

import com.pkpservice.orderservice.model.entity.CartEntity;
import com.pkpservice.orderservice.model.entity.CartItemEntity;
import com.pkpservice.orderservice.model.request.CartItemRequest;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CartResponse {
    @NotNull
    private String username;
    @NotNull
    private String customerId;
    @NotNull
    private String customerName;
    @NotNull
    private String invoiceNo;
    @NotNull
    private List<CartItemResponse> cartItems;

    public CartResponse(CartEntity entity) {
        BeanUtils.copyProperties(entity, this);
        if(!entity.getCartItems().isEmpty()){
            this.cartItems = entity.getCartItems().stream()
                    .map(CartItemResponse::new)
                    .collect(Collectors.toList());
        }
    }
}
