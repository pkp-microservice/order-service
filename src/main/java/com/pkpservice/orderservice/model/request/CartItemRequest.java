package com.pkpservice.orderservice.model.request;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CartItemRequest {
    @NotNull
    private Long productId;
    @NotNull
    private Long price;
    @NotNull
    private Long qty;
    @NotNull
    private String description;
}
