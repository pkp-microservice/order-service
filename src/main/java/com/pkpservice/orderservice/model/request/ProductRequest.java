package com.pkpservice.orderservice.model.request;

import com.pkpservice.orderservice.model.entity.ProductEntity;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;
import org.springframework.beans.BeanUtils;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductRequest {
    private Long id;

    @NotBlank(message = "Name tidak boleh kosong")
    @Length(min = 5, max = 100, message = "Name Minimal 5 karakter")
    private String name;

    @NotBlank(message = "Description tidak boleh kosong")
    private String description;

    @NotNull(message = "Price tidak boleh kosong")
    private Long price;

    @NotNull(message = "Category id tidak boleh kosong")
    private Long categoryId;

    @NotNull(message = "Stock tidak boleh kosong")
    private Long stock;
}
