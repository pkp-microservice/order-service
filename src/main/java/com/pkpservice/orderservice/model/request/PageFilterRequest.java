package com.pkpservice.orderservice.model.request;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PageFilterRequest {
    @NotNull(message = "not nul")
    @Min(value = 1, message = "Minimal value is 1")
    private int pageSize;

    @NotNull
    @Min(value = 0, message = "Minimal value is 0")
    private int page;
    private String keySearch;
}
