package com.pkpservice.orderservice.model.request;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CategoryRequest {
    private Long id;
    @NotBlank(message = "Code tidak boleh kosong")
    @Length(min = 3, max = 10, message = "Mininaml 3 karakter")
    private String code;

    @NotBlank(message = "Name tidak boleh kosong")
    @Length(min = 5, max = 120, message = "Minimal 5 karakter, maksimal 10 karakter")
    private String name;
}
