package com.pkpservice.orderservice.model.request;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CartRequest {
    @NotNull
    private String username;
    @NotNull
    private String customerId;
    @NotNull
    private String customerName;
    @NotNull
    private String invoiceNo;
    @NotNull
    private List<CartItemRequest> cartItems;
}
