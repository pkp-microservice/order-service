package com.pkpservice.orderservice.model.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.pkpservice.orderservice.model.request.CartItemRequest;
import com.pkpservice.orderservice.model.request.CartRequest;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "tbl_cart")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CartEntity {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "username", length = 64)
    private String username;

    @Column(name = "customer_id", length = 100)
    private String customerId;

    @Column(name = "customer_name", length = 100)
    private String customerName;

    @Column(name = "invoice_no", length = 20, unique = true)
    private String invoiceNo;

    @Column(name = "total")
    private Double total;

    @Column(name = "created_at")
    private LocalDateTime createdAt;

    @Column(name = "status", length = 32)
    private String status;

    @OneToMany(mappedBy = "cart",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @JsonManagedReference
    private List<CartItemEntity> cartItems = new ArrayList<>();

    public void addCartItem(CartItemEntity itemEntity){
        this.cartItems.add(itemEntity);
        itemEntity.setCart(this);
    }

    public void removeCartItem(CartItemEntity itemEntity){
        this.cartItems.remove(itemEntity);
        itemEntity.setCart(null);
    }

    public CartEntity(CartRequest cartDto) {
        this.customerName = cartDto.getCustomerName();
        this.invoiceNo = cartDto.getInvoiceNo();
        this.createdAt = LocalDateTime.now();

        Double vTotal = 0.0;
        for(CartItemRequest itemDto: cartDto.getCartItems()){
            CartItemEntity itemEntity = new CartItemEntity(itemDto, this);
            this.addCartItem(itemEntity);
            vTotal+= itemEntity.getSubTotal();
        }
        this.total = vTotal;
    }
}
