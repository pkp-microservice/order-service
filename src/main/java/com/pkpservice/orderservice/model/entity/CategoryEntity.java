package com.pkpservice.orderservice.model.entity;

import com.pkpservice.orderservice.model.request.CategoryRequest;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "tbl_category")
public class CategoryEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "category_id")
    private Long id;

    @Column(name = "category_code", length = 10)
    private String code;

    @Column(name = "category_name", length = 128)
    private String name;

    @OneToMany(mappedBy = "category")
    private List<ProductEntity> products = new ArrayList<>();

    public CategoryEntity(CategoryRequest model) {
        BeanUtils.copyProperties(model, this);
    }

    public CategoryEntity(String code, String name) {
        this.code = code;
        this.name = name;
    }
}
