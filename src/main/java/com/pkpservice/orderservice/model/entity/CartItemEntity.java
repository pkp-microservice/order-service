package com.pkpservice.orderservice.model.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.pkpservice.orderservice.model.request.CartItemRequest;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Entity
@Table(name = "tbl_cart_item")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CartItemEntity {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "card_id", insertable = false, updatable = false)
    private Long cartId;

    @Column(name = "product_id", nullable = false)
    private Long productId;

    @Column(name = "qty")
    private Long qty;

    @Column(name = "price")
    private Long price;

    @Column(name = "sub_total")
    private Long subTotal;

    @Column(name = "description")
    private Long description;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "product_id", insertable = false, updatable = false)
    @JsonBackReference
    private ProductEntity product;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "card_id", nullable = false)
    @JsonBackReference
    private CartEntity cart;

    public CartItemEntity(CartItemRequest itemDto, CartEntity cart) {
        this.cart = cart;
        this.productId = itemDto.getProductId();
        this.qty = itemDto.getQty();
        this.price = itemDto.getPrice();
        this.subTotal = itemDto.getQty() * itemDto.getPrice();
    }
}
