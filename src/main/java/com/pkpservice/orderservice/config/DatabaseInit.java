package com.pkpservice.orderservice.config;

import com.pkpservice.orderservice.model.entity.CategoryEntity;
import com.pkpservice.orderservice.repository.CategoryRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Slf4j
@Component
@RequiredArgsConstructor
public class DatabaseInit implements CommandLineRunner {
    private final CategoryRepository categoryRepo;
    @Override
    public void run(String... args) throws Exception {
        initCategory();
        initProduct();
    }

    private void initCategory(){
        if(categoryRepo.count() > 0){
            return;
        }

        try{
            this.categoryRepo.saveAll(Arrays.asList(
                    new CategoryEntity("C001", "Makanan"),
                    new CategoryEntity("C002", "Minuman"),
                    new CategoryEntity("C003", "Elektronik"),
                    new CategoryEntity("C004", "Fashion"),
                    new CategoryEntity("C005", "Smart phone")
            ));
            log.info("Save category success");
        }catch (Exception e){
            log.error("Save category failed, error: {}", e.getMessage());
        }
    }

    private void initProduct(){

    }
}
